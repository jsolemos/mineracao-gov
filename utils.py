import json
import pydash as _


def read_json_file(filename):
    with open(filename, "r") as read_file:
        return json.load(read_file)


def param_case(s):
    return _.snake_case(_.deburr(s))


