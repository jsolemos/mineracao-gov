import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import re
import pydash as _
from textblob.classifiers import NaiveBayesClassifier
from textblob import TextBlob
from textblob.exceptions import TranslatorError, NotTranslated
import nltk
nltk.download('stopwords')
from urllib.request import urlopen
import json
import time
from pathlib import Path
from wordcloud import WordCloud
import tweepy
from tweepy.error import TweepError, RateLimitError

# Carrega variáveis de ambiente
with open('./.env') as f:
    # print(f.read())
    for line in f:
        new_env = line.replace('export ', '', 1).strip().split('=', 1)
        os.environ[new_env[0]] = new_env[1]
    # os.environ.update(
    #     line.replace('export ', '', 1).strip().split('=', 1)
    #     if 'export' in line
    # )

# print(os.environ)

# Configuração da api Twitter

APP_KEY = os.environ['APP_KEY']
APP_SECRET = os.environ['APP_SECRET']
OAUTH_TOKEN = os.environ['OAUTH_TOKEN']
OAUTH_TOKEN_SECRET = os.environ['OAUTH_TOKEN_SECRET']


def createTwiterClient():
    auth = tweepy.OAuthHandler(APP_KEY, APP_SECRET)
    auth.set_access_token(OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    api = tweepy.API(auth, wait_on_rate_limit=True)
    print(api.rate_limit_status()['resources']['statuses']['/statuses/user_timeline'])
    return api;

global twitter
twitter = createTwiterClient()



# Funções para manipulação dos textos
def PreprocessamentoSemStopWords(instancia):
    #remove links dos tweets
    #remove stopwords
    instancia = re.sub(r"http\S+", "", instancia).lower().replace(',','').replace('.','').replace(';','').replace('-','')
    stopwords = set(nltk.corpus.stopwords.words('portuguese'))
    palavras = [i for i in instancia.split() if not i in stopwords]
    return (" ".join(palavras))

def Stemming(instancia):
    stemmer = nltk.stem.RSLPStemmer()
    palavras=[]
    for w in instancia.split():
        palavras.append(stemmer.stem(w))
    return (" ".join(palavras))

def Preprocessamento(instancia):
    #remove links, pontos, virgulas,ponto e virgulas dos tweets
    #coloca tudo em minusculo
    instancia = re.sub(r"http\S+", "", instancia).lower().replace(',','').replace('.','').replace(';','').replace('-','').replace(':','')
    return (instancia)


def translate(s):
    try:
        blob = TextBlob(s)
        return str(blob.translate(to="en"))
    except Exception as e:
        print("-", end="")
        return s


def get_sentimento(s):
    #     blob = TextBlob(s)
    str_translaterd = translate(s)
    blob = TextBlob(str_translaterd)
    out = {
        "frase": s,
        "translation": str_translaterd,
        "polarity": blob.polarity,
        "subjetividade": blob.subjectivity
    }
    if blob.polarity > 0:
        out["class"] = "positivo"
    elif blob.polarity < 0:
        out["class"] = "negativo"
    elif blob.polarity == 0:
        out["class"] = "neutro"
    return out


def req_tweets(user, n_tweets=20):
    global twitter
    try:
        status = twitter.user_timeline(screen_name=user, count=n_tweets)
        return _.map_(status, lambda x: x._json)
    except TweepError as e:
        #         twitter = createTwiterClient()
        print(e, "Não foi possível consultar os tweets")
        #         time.sleep(randint(3, 20))
        #         return req_tweets(user)
        return []


def get_new_tweets(user, n_tweets=20):
    jsonfileout = Path(f'db/{user}-tweets.json')
    tweets = req_tweets(user, n_tweets)
    with open(jsonfileout, 'w') as f:
        json.dump(fp=f, obj=tweets)
    return tweets


def only_text_of_tweets(tweets):
    return np.array(_.map_(tweets, lambda x: x['text']))


# Função para Recupera os tweets feitos pelo usuario
def get_tweets(user, n_tweets=20):
    jsonfileout = Path(f'db/{user}-tweets.json')
    if jsonfileout.exists():
        with open(jsonfileout, 'r') as f:
            json_d = json.load(f)
            if len(json_d) != n_tweets:  # Caso o tamanho dos itens seja diferente do solicitado, busca no twitter
                tweets = get_new_tweets(user, n_tweets)
            else:
                tweets = json_d
    else:
        tweets = get_new_tweets(user, n_tweets)

    return only_text_of_tweets(tweets)

def RemoviStopWords(instancia):
    instancia = instancia.lower()
    stopwords = set(nltk.corpus.stopwords.words('portuguese'))
    palavras = [i for i in instancia.split() if not i in stopwords]
    return (" ".join(palavras))

def clean_tweet(text):
    return re.sub(r'[-./?!,":;()\']', '', RemoviStopWords(text))

def analyse_sentiments(user, n_tweets=20):
    print("Bucando tweets de", user)
    user_tweets = get_tweets(user, n_tweets)
    if len(user_tweets) > 0:
        print ("Analisando os tweets de ", user)
    classes = _.map_(user_tweets, lambda x: get_sentimento(clean_tweet(x)))
    print (".")
    return classes


def classify_sentiments(user, n_tweets=20):
    print("Classificação dos tweets do usuário {}".format(user))
    dbPath = Path(f'db')
    if not dbPath.exists():
        dbPath.mkdir()
    fileclassesout = Path(f'{dbPath}/classes-{user}.csv')

    if fileclassesout.exists():
        df = pd.read_csv(fileclassesout)
        df["UsuarioTwitter"] = user
        c = df["class"].value_counts().sort_index()
        c.plot.pie(autopct='%1.0f%%')
        plt.title(f"Classificação dos últimos {n_tweets} tweets de {user}")
        plt.axis("equal")
        print(c)
        save = False
        if (not (df.empty) and "polarity" not in df):
            save = True
            df["polarity"] = df["translation"].apply(lambda x: get_sentimento(x)["polarity"])
        if (save):
            df.to_csv(fileclassesout, index=False)

        if not (df.empty):
            # Consulta e classifica os tweets de um usuário
            showCloudTags(df)
        return df
    else:
        df = pd.DataFrame(analyse_sentiments(user, n_tweets))

    if df.empty:
        return df

    df.to_csv(fileclassesout, index=False)
    df["UsuarioTwitter"] = user
    c = df["class"].value_counts().sort_index()
    c.plot.pie(autopct='%1.0f%%')
    plt.title(f"Classificação dos últimos {n_tweets} tweets de {user}")
    plt.axis("equal")
    plt.show()
    print(c)
    if not (df.empty):
        # Consulta e classifica os tweets de um usuário
        showCloudTags(df)
    return df

def showCloudTags(df):
    if("frase" in df):
        # print(df)
        text = re.sub(r"http\S+", "", " ".join(_.map_(df['frase'].unique(), lambda x: str(x))))
        wordcloud = WordCloud(max_font_size=100,width = 1520, height = 800).generate(text)
        plt.figure(figsize=(16, 9))
        plt.title(f"Assuntos dos últimos tweets de {df['UsuarioTwitter'].head(1)[0]}")
        plt.imshow(wordcloud)
        plt.axis("off")
        plt.show()

def trataRedeSocial(x):
    tweetMatch = re.search(r'(http(?:s)?:\/\/)?(?:www\.)?twitter\.com\/@?([a-zA-Z0-9_]+)', str(x))
    if tweetMatch:
        return tweetMatch.group(2)
    return ''


print('Um simples Exemplo de classificação de uma frase.')
frase_exemplo = get_sentimento("O governo está mal desorganizado")
print(frase_exemplo)

res_deputados = urlopen("https://dadosabertos.camara.leg.br/arquivos/deputados/csv/deputados.csv")

deputados = pd.read_csv(res_deputados, delimiter=";",
                        usecols=[1,7, 11],
                        converters={
    'urlRedeSocial': lambda x: trataRedeSocial(x)
})
twitter_deps = deputados[deputados['urlRedeSocial'] != '']
twitter_deps.columns = ["Nome", "UsuarioTwitter", "UF"]

print('Visão dos dados')
print('5 primeiros Parlamentares da lista')
print(twitter_deps.head())
print('Total de parlamentares com a rede social twitter cadastrado nos dados abertos do governo.')
print(twitter_deps["UsuarioTwitter"].count())

classications = []

# somente os da bahia
deps_ba = twitter_deps[twitter_deps['UF'] == "BA"]
print("Na bahia, um total de ", deps_ba['UsuarioTwitter'].count())
print(deps_ba)

# # Exibe a classifica e assuntos mais falados dos deputados da bahia.
for user in deps_ba.UsuarioTwitter:
    # Consulta e classifica os tweets de um usuário
    classify_sentiments(user, 20)

# Ou usar qualquer usuário do twitter
# Como exemplo, vamos ver também da Faculdade Dom Pedro II
classify_sentiments("facdompedroii", 20)