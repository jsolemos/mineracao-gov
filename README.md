
##### Pré-requisitos
[Instalar o pip e virtualenv](https://pythonacademy.com.br/blog/python-e-virtualenv-como-programar-em-ambientes-virtuais)


##### Configurar o projeto

Com o pip e virtualenv já instalados, execute o comando abaixo para criar o virtualenv e instalar as dependências.

    $ ./configure.sh
 

Ou [Configure o virtual-environment no Pycharm](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html)

Todas as dependencias estão no arquivo **requeriments.txt**

## Classificação de tweets dos parlamentares
São buscados os n últimos tweets feitos pelos parlamentares, e são classificados como Neutro, Positivo ou Negativo. Serão exibidos gráficos e uma nuvem de tags com os assuntos mais twitados por cada um deles.

OBS: No script estão filtrados apenas os parlamentares da bahia para exibir

execute o classify_tweets.py pelo pycharm ou via linha de comando:

    $ python ./classify_tweets.py 

##### Breve explicação do funcionamento
- São buscados os usuários do twitter dos parlamentares nos site de [Dados abertos da Câmara dos Deputados](https://dadosabertos.camara.leg.br)
- São transformados para melhor manipulação e análise e salvos localmente para melhor desempenho nas próximas execuções
- O mesmo processo é feito para consultar os tweets de cada parlamentar
- No processo de classificação, é usado o algorítimo NaiveBayesClassifier já treinado com frases em inglês a partir da biblioteca TextBlob
- Cada tweet é traduzido para o inglês e classificado 
- Ao final são unidos todos os tweets para verificar a quantidade de ocorrências das palavras e são exibidos os gráficos e a nuvem de tags 

## CLI para consultar informações sobre gastos de deputados


##### Comandos

    $ python ./main.py --help
    Usage: main.py [OPTIONS] COMMAND [ARGS]...
    
      CLI para consultar informações sobre gastos de candidatos
    
    Options:
      --help  Show this message and exit.
    
    Commands:
      buscar    Filtrar candidatos
      detalhar  Detalha os gastos de um deputado a partir de seu número...

##### Buscar candidados

    $ python ./main.py buscar --help
    Usage: main.py buscar [OPTIONS]
    
      Filtrar candidatos
    
    Options:
      -a, --ano INTEGER   Ano da apuração dos dados
      -n, --nome TEXT     Nome completo do candidato ou parte dele
      -p, --partido TEXT  Sigla do partido
      --help              Show this message and exit.


##### Detalhar candidado

    $ python ./main.py detalhar --help
    Usage: main.py detalhar [OPTIONS] ID
    
      Detalha os gastos de um deputado a partir de seu número nuDeputadoId
    
    Options:
      -a, --ano INTEGER  Ano da apuração dos dados
      --help             Show this message and exit.

